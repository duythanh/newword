﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Collections.ObjectModel;
using APP_NEWWORDS.Models;
using APP_NEWWORDS.ViewModels;

namespace APP_NEWWORDS.Views
{
    public partial class QuizLinkWordPage : PhoneApplicationPage
    {
        private ObservableCollection<Word> nameList;
        private ObservableCollection<Word> desList;
        private ObservableCollection<Word> nameDesList;
        List<Word> obserWord;
        private List<string> BangMau;
        private Button leftCol = null;
        private Button rightCol = null;
        private ListBox preLB = null;
        private int wordToLearn = 0;
        private int icorrect = 0;
        private int iwrong;
        private Color[] myColor = { Color.FromArgb(255, 240, 163, 10), Color.FromArgb(255, 0, 138, 0), Color.FromArgb(255, 118, 96, 138), Color.FromArgb(255, 135, 121, 78), 
                                Color.FromArgb(255, 130, 90, 44), Color.FromArgb(255, 96, 169, 23), Color.FromArgb(255, 109, 135, 100), Color.FromArgb(255, 0, 171, 169),
                                Color.FromArgb(255, 162, 0, 37), Color.FromArgb(255, 106, 0, 255), Color.FromArgb(255, 250, 104, 0), Color.FromArgb(255, 170, 0, 255),
                                Color.FromArgb(255, 0, 80, 239), Color.FromArgb(255, 164, 196, 0), Color.FromArgb(255, 244, 114, 208), Color.FromArgb(255, 227, 200, 0),
                                Color.FromArgb(255, 27, 161, 226), Color.FromArgb(255, 216, 0, 115), Color.FromArgb(255, 100, 118, 135)};
        private int round = 0;
        public QuizLinkWordPage()
        {
            InitializeComponent();        
        }

        private Color GenerateColor()
        {            
            round++;
            return myColor[round%18];         
        }

        private void display()
        {
            icorrect = 0;
            iwrong = 2;
            randoomList();

            this.gameList.ItemsSource = nameDesList;
            this.gameList.IsHitTestVisible = true;
            this.correct.Text = "0 / " + wordToLearn;
            List<Button> lstBtn = wplChances.Children.OfType<Button>().ToList();
            foreach (Button btn in lstBtn)
            {
                btn.Visibility = System.Windows.Visibility.Visible;
            }
        }
        
        private void randoomList()
        {
            nameList = new ObservableCollection<Word>();
            desList = new ObservableCollection<Word>();
            nameDesList = new ObservableCollection<Word>();
            obserWord = WordViewModel.WordList.ToList();
            // lay ra N so tu can choi
            Random rd = new Random();
            int i = 0;            
            while (i < wordToLearn)
            {
                int index = rd.Next(0, obserWord.Count - 1);
                Word word = obserWord[index];
                obserWord.RemoveAt(index);
                    nameList.Add(word.GetCopy());
                    nameDesList.Add(word.GetCopy());
                    i++;
            }

             
            List<Word> listWord = nameList.ToList();
            
            // tao random vi tri Desscription cho moi tu
            i = 0;
            while (i < wordToLearn)
            {
                int index = rd.Next(0, listWord.Count - 1);
                Word word = listWord[index];
                listWord.RemoveAt(index);
                desList.Add(word.GetCopy());
                i++;
            }

            for (i = 0; i < nameDesList.Count; i++)
            {
                nameDesList[i].Transl = desList[i].Transl;
            }

        }


        private void btnLearn_Click(object sender, RoutedEventArgs e)
        {
            string value = txtWordToLearn.Text;
            if (Regex.IsMatch(value, @"^[0-9]+$"))
            {
                int n = int.Parse(value);
                if (n > WordViewModel.WordList.Count)
                {
                    wordToLearn = WordViewModel.WordList.Count;
                    txtWordToLearn.Text = wordToLearn.ToString();
                }
                else
                    wordToLearn = n;
                display();
            }
            else
            {
                MessageBox.Show("Invalid number !");
                txtWordToLearn.Text = "0";
            }
        }

        private void txtWordToLearn_GotFocus(object sender, RoutedEventArgs e)
        {
            txtWordToLearn.SelectAll();

            this.resetCurrentControlClicked();
        }

        private void txtWordToLearn_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
             e.Handled = !("D1D2D3D4D5D6D7D8D9D0".Contains(e.Key.ToString())); 
        }

        private void btnLeft_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            e.Handled = true;

            if (rightCol == null)
            {
                if (leftCol == null)
                {
                    leftCol = sender as Button;
                    leftCol.Foreground = new SolidColorBrush(Colors.Red);
                    leftCol.BorderBrush = new SolidColorBrush(Colors.Red);
                }
                else
                {
                    leftCol.Foreground = new SolidColorBrush(Colors.Black);
                    leftCol.BorderBrush = new SolidColorBrush(Colors.Black);

                    leftCol = sender as Button;
                    leftCol.Foreground = new SolidColorBrush(Colors.Red);
                    leftCol.BorderBrush = new SolidColorBrush(Colors.Red);
                }

            }
            else // khi co doi da dc chon
            {
                leftCol = sender as Button;

                Word leftWord = (Word)leftCol.Tag;
                Word wordNameDes = nameList.FirstOrDefault(x => x.Name == leftWord.Name);

                Word rightWord = (Word)rightCol.Tag;

                // tra loi dung
                if (wordNameDes.Transl.Equals(rightWord.Transl) == true)
                {
                    this.borderCurrentControl();

                    icorrect++;
                    this.correct.Text = icorrect + " / " + wordToLearn;

                    if (icorrect == wordToLearn)
                    {
                        MessageBox.Show("Your choices are correct !");
                    }
                }
                else // tra loi sai
                {
                    resetCurrentControlClicked();

                    if (iwrong > -1)
                    {
                        List<Button> lstBtn = wplChances.Children.OfType<Button>().ToList();
                        lstBtn[iwrong].Visibility = System.Windows.Visibility.Collapsed;
                        iwrong--;
                    }

                    if (iwrong == -1)
                    {
                        MessageBox.Show("Your choices are incorrect !");
                        this.gameList.IsHitTestVisible = false;
                    }
                }

            }
        }

        private void ListBox_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            e.Handled = false;
            List<Translate> transl1;
            if (preLB != null)
            {
                transl1 = preLB.Tag as List<Translate>;
                preLB.ItemsSource = null;
                preLB.Foreground = new SolidColorBrush(Colors.Black);
                preLB.ItemsSource = transl1;
            }

            ListBox listb = sender as ListBox;
            transl1 = listb.Tag as List<Translate>;

            listb.ItemsSource = null;
            listb.Foreground = new SolidColorBrush(Colors.Red);
            listb.ItemsSource = transl1;

            preLB = listb;
            if (leftCol == null)
            {
                if (rightCol == null)
                {
                    rightCol = listb.Parent as Button;
                    rightCol.BorderBrush = new SolidColorBrush(Colors.Red);
                }
                else
                {
                    rightCol.BorderBrush = new SolidColorBrush(Colors.Black);

                    rightCol = listb.Parent as Button;
                    rightCol.BorderBrush = new SolidColorBrush(Colors.Red);
                }
            } // khi doi phuong da chon
            else
            {
                rightCol = listb.Parent as Button;

                Word leftWord = (Word)leftCol.Tag;
                Word wordNameDes = nameList.FirstOrDefault(x => x.Name == leftWord.Name);
                Word rightWord = (Word)rightCol.Tag;

                // tra loi dung
                if (wordNameDes.Transl.Equals(rightWord.Transl) == true)
                {
                    this.borderCurrentControl();

                    icorrect++;
                    this.correct.Text = icorrect + " / " + wordToLearn;

                    if (icorrect == wordToLearn)
                    {
                        MessageBox.Show("You win !");
                    }
                }
                else // tra loi sai
                {
                    resetCurrentControlClicked();

                    if (iwrong > -1)
                    {
                        List<Button> lstBtn = wplChances.Children.OfType<Button>().ToList();
                        lstBtn[iwrong].Visibility = System.Windows.Visibility.Collapsed;
                        iwrong--;
                    }

                    if (iwrong == -1)
                    {
                        MessageBox.Show("You lose !");
                        this.gameList.IsHitTestVisible = false;
                    }
                }
            }
            
        }

        private void borderCurrentControl()
        {
            Color win = this.GenerateColor();
            leftCol.BorderBrush = new SolidColorBrush(win);
            leftCol.Background = new SolidColorBrush(win);
            leftCol.Foreground = new SolidColorBrush(Colors.White);

            rightCol.BorderBrush = new SolidColorBrush(win);
            rightCol.Background = new SolidColorBrush(win);


            
            if (preLB != null)
            {
                List<Translate> transl1;
                transl1 = preLB.Tag as List<Translate>;
                preLB.ItemsSource = null;
                preLB.Foreground = new SolidColorBrush(Colors.White);
                preLB.ItemsSource = transl1;
                preLB = null;
            }

            leftCol.IsHitTestVisible = false;
            rightCol.IsHitTestVisible = false;

            leftCol = null;
            rightCol = null;
        }

        private void resetCurrentControlClicked()
        {
            if (preLB != null)
            {
                List<Translate> transl1 = preLB.Tag as List<Translate>;
                preLB.ItemsSource = null;
                preLB.Foreground = new SolidColorBrush(Colors.Black);
                preLB.ItemsSource = transl1;
                preLB = null;
            }

            if (leftCol != null)
            {
                leftCol.Foreground = new SolidColorBrush(Colors.Black);
                leftCol.BorderBrush = new SolidColorBrush(Colors.Black);
                leftCol = null;
            }

            if (rightCol != null)
            {
                rightCol.Foreground = new SolidColorBrush(Colors.Black);
                rightCol.BorderBrush = new SolidColorBrush(Colors.Black);
                rightCol = null;
            }
        }

        private void LayoutRoot_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            e.Handled = true;

            if (e.OriginalSource.GetType() != typeof(TextBlock))
            {
                resetCurrentControlClicked();
            }
            else
            {
                TextBlock tb = e.OriginalSource as TextBlock;
                if (!(tb.Name == "txtLeft" || tb.Name == "txtRight"))
                {
                    resetCurrentControlClicked();
                }
            }
        } 
    }
}