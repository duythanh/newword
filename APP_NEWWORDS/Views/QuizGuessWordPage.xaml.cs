﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Documents;
using System.Windows.Media;
using System.Text.RegularExpressions;
using APP_NEWWORDS.Models;
using APP_NEWWORDS.ViewModels;

namespace APP_NEWWORDS.Views
{
    public partial class QuizGuessWordPage : PhoneApplicationPage
    {
        bool isAgain;
        int numOfCorrect = 0;
        Dictionary<Word, bool> dctAnswer = new Dictionary<Word, bool>();

        private int indexWordForGame;
        private int iWrong;
        private int iLenOfWord;
        private Random rd;
        private Color[] myColor = { Colors.Blue, Colors.Green, Colors.Orange, Colors.Yellow, Colors.Gray };

        public QuizGuessWordPage()
        {
            InitializeComponent();
            init();
        }


        private void init()
        {
            indexWordForGame = 0;
            iWrong = 3;
            numOfCorrect = 0;
            rd = new Random();
            int rdIndex;

            Dictionary<Word, bool> dctWord = new Dictionary<Word, bool>();

            foreach (Word word in WordViewModel.WordList)
            {
                for(int i = 0; i < word.Transl.Count; i++)
                {                   
                    Word tmp = new Word(word.Name, word.LngFrom, word.EnDescription, word.Images, new List<Translate> { word.Transl[i]});
                    dctWord.Add(tmp.GetCopy(), true);
                }
            }

            int maxLength = dctWord.Count;
            for (int i = 0; i < maxLength; i++)
            {
                rdIndex = rd.Next(0, dctWord.Count - 1);
                dctAnswer.Add(dctWord.ElementAt(rdIndex).Key, true);
                dctWord.Remove(dctWord.ElementAt(rdIndex).Key);
            }

            //this.txtSuggestion.Text = "Suggestion: (" + dctAnswer.ElementAt(indexWordForGame).Key.LngFrom + ")";
            //this.txtDescription.Text =  "(" + dctAnswer.ElementAt(indexWordForGame).Key.Transl[0].LngTo + ") " + dctAnswer.ElementAt(indexWordForGame).Key.Transl[0].Description;
            //this.txtNote.Text = dctAnswer.ElementAt(indexWordForGame).Key.Transl[0].Note;
           
            this.skpSuggestion.DataContext = dctAnswer.ElementAt(indexWordForGame).Key;

            isAgain = true;
            string strWord = dctAnswer.ElementAt(indexWordForGame).Key.Name;
            iLenOfWord = strWord.Length;
            wplWord.Children.Clear();
            for (int i = 0; i < strWord.Length; i++)
            {
                Button btnLetter = new Button();
                btnLetter.Height = 80;
                btnLetter.Width = 80;
                btnLetter.Content = strWord[i].ToString();

                if (strWord[i] == ' ' || strWord[i] == '-')
                {
                    btnLetter.Background = new SolidColorBrush(Colors.White);
                    btnLetter.Foreground = new SolidColorBrush(Colors.Black);
                    iLenOfWord--;
                }
                else
                {
                    btnLetter.Background = new SolidColorBrush(myColor[2]);
                    btnLetter.Foreground = new SolidColorBrush(Colors.Transparent);

                }

                btnLetter.IsHitTestVisible = false;
                btnLetter.Margin = new Thickness(-10, 0, -10, 0);
                wplWord.Children.Add(btnLetter);
            }

            this.tbCorrect.Text = "0 / " + dctAnswer.Count;
        }

        private void btnGuessLetter_Click(object sender, RoutedEventArgs e)
        {
            bool isCorrect = false;
            if (txtGuessLetter.Text.Trim().Length > 0)
            {
                string strWord = dctAnswer.ElementAt(indexWordForGame).Key.Name;
                string strLetter = txtGuessLetter.Text;

                for (int i = 0; i < strWord.Length; i++)
                    if (strWord[i].ToString().ToLower() == strLetter.ToLower())
                    {
                        isCorrect = true;
                        foreach (Button btnTmp in wplWord.Children.OfType<Button>().ToList())
                        {
                            if (btnTmp.Content.ToString().ToLower() == strWord[i].ToString().ToLower())
                            {
                                btnTmp.Foreground = new SolidColorBrush(Colors.Black);
                                iLenOfWord--; // giam so luong letter con phai tra loi
                            }
                        }

                        strWord = Regex.Replace(strWord, strWord[i].ToString(), " ", RegexOptions.IgnoreCase);

                        txtGuessLetter.Text = "";
                        txtGuessLetter.Focus();
                    }

                if (iLenOfWord == 0)
                {
                    MessageBox.Show("The answer is correct !");
                    this.btnGuessLetter.Focus();
                    proceesLoseOrWin();
                    
                    isAgain = false;
                    dctAnswer[dctAnswer.ElementAt(indexWordForGame).Key] = false;
                    numOfCorrect++;
                    this.tbCorrect.Text = numOfCorrect + " / " + dctAnswer.Count;
                }
                // neu truong hop tra loi sai 
                if (!isCorrect)
                {
                    List<Button> lstBtn = wplChances.Children.OfType<Button>().ToList();
                    iWrong--;
                    lstBtn[iWrong].Visibility = System.Windows.Visibility.Collapsed;

                    txtGuessLetter.Text = "";
                    txtGuessLetter.Focus();

                    if (iWrong == 0)
                    {
                        lstBtn[iWrong].Visibility = System.Windows.Visibility.Collapsed;
                        MessageBox.Show("The answer is incorrect !");
                        proceesLoseOrWin();
                        txtAnswer.Focus();
                        isAgain = false;
                        dctAnswer[dctAnswer.ElementAt(indexWordForGame).Key] = false;
                    }
                }
            }


        }

        private void btnQuickAnswer_Click(object sender, RoutedEventArgs e)
        {
            if (txtAnswer.Text.Length > 0)
            {
                string strWord = dctAnswer.ElementAt(indexWordForGame).Key.Name.ToLower();
                string strAnswer = txtAnswer.Text.Trim().ToLower();


                if (strWord == strAnswer)
                {
                    MessageBox.Show("The answer is correct !");
                    proceesLoseOrWin();
                    isAgain = false;
                    dctAnswer[dctAnswer.ElementAt(indexWordForGame).Key] = false;
                    numOfCorrect++;
                    this.tbCorrect.Text = numOfCorrect + " / " + dctAnswer.Count;
                }
                else
                {
                    List<Button> lstBtn = wplChances.Children.OfType<Button>().ToList();
                    iWrong--;
                    lstBtn[iWrong].Visibility = System.Windows.Visibility.Collapsed;

                    txtAnswer.Text = "";
                    txtAnswer.Focus();

                    if (iWrong == 0)
                    {
                        lstBtn[iWrong].Visibility = System.Windows.Visibility.Collapsed;
                        MessageBox.Show("The answer is incorrect !");
                        proceesLoseOrWin();
                        this.btnQuickAnswer.Focus();
                        isAgain = false;
                        dctAnswer[dctAnswer.ElementAt(indexWordForGame).Key] = false;
                    }
                }


            }
        }

        private void proceesLoseOrWin()
        {
            btnGuessLetter.IsHitTestVisible = false;
            btnQuickAnswer.IsHitTestVisible = false;

            txtGuessLetter.IsHitTestVisible = false;

            txtAnswer.IsHitTestVisible = false;

            foreach (Button btnTmp in wplWord.Children.OfType<Button>().ToList())
            {
                btnTmp.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void displayNewQuestion()
        {
            //this.txtSuggestion.Text = "Suggestion: (" + dctAnswer.ElementAt(indexWordForGame).Key.LngFrom + ")";
            //this.txtDescription.Text = "(" + dctAnswer.ElementAt(indexWordForGame).Key.Transl[0].LngTo + ") " + dctAnswer.ElementAt(indexWordForGame).Key.Transl[0].Description;
            //this.txtNote.Text = dctAnswer.ElementAt(indexWordForGame).Key.Transl[0].Note;
            this.skpSuggestion.DataContext = dctAnswer.ElementAt(indexWordForGame).Key;

            isAgain = true;
            string strWord = dctAnswer.ElementAt(indexWordForGame).Key.Name;
            iLenOfWord = strWord.Length;

            txtAnswer.Text = "";
            txtGuessLetter.Text = "";
            txtGuessLetter.IsHitTestVisible = true;
            txtAnswer.IsHitTestVisible = true;
            btnGuessLetter.IsHitTestVisible = true;
            btnQuickAnswer.IsHitTestVisible = true;
            wplWord.Children.Clear();

            for (int i = 0; i < strWord.Length; i++)
            {
                Button btnLetter = new Button();
                btnLetter.Height = 80;
                btnLetter.Width = 80;
                btnLetter.Content = strWord[i].ToString();

                if (strWord[i] == ' ' || strWord[i] == '-')
                {
                    btnLetter.Background = new SolidColorBrush(Colors.White);
                    btnLetter.Foreground = new SolidColorBrush(Colors.Black);
                    iLenOfWord--;
                }
                else
                {
                    btnLetter.Background = new SolidColorBrush(myColor[2]);
                    btnLetter.Foreground = new SolidColorBrush(Colors.Transparent);

                }

                btnLetter.IsHitTestVisible = false;
                btnLetter.Margin = new Thickness(-10, 0, -10, 0);
                wplWord.Children.Add(btnLetter);
            }
        }

        private void btnIgnore_Click(object sender, EventArgs e)
        {
            indexWordForGame++;
            if (indexWordForGame < dctAnswer.Count)
            {
                List<Button> lstBtn = wplChances.Children.OfType<Button>().ToList();
                foreach (Button btn in lstBtn)
                    btn.Visibility = System.Windows.Visibility.Visible;
                displayNewQuestion();
            }
            else
            {
                MessageBox.Show("You got " + numOfCorrect + " out of " + dctAnswer.Count + " questions");
                NavigationService.Navigate(new Uri("/Views/QuizPage.xaml", UriKind.Relative));
            }
        }
    }
}