﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using APP_NEWWORDS.ViewModels;
using APP_NEWWORDS.Models;
using System.Threading;
using Microsoft.Phone.Net.NetworkInformation;
using System.Diagnostics;
using System.Collections.ObjectModel;
using System.IO.IsolatedStorage;

namespace APP_NEWWORDS.Views
{
    public partial class AddWordPage : PhoneApplicationPage
    {
        private static LngViewModel _lngvm;
        private IsolatedStorageSettings _settings = IsolatedStorageSettings.ApplicationSettings;
        List<Language> _lngToItem;
        private string _name = string.Empty;
        private string _lngFrom = string.Empty;
        private string _lngTo = string.Empty;
        private int _fromOrTo = 1;
        public AddWordPage()
        {
            InitializeComponent();
            initTranslateLng();
        }

        private void initTranslateLng()
        {
            if(_lngvm == null)
                _lngvm = new LngViewModel();

            int indexWord = -1;

            if (PhoneApplicationService.Current.State.ContainsKey("IndexWord"))
            {
                indexWord = (int)PhoneApplicationService.Current.State["IndexWord"];
                _name = WordViewModel.WordList[indexWord].Name;
                _lngFrom = WordViewModel.WordList[indexWord].LngFrom;
            }

            this.sltLngFrom.ItemsSource = _lngvm.LngItemVM;

            if (_lngFrom != string.Empty)
            {
               // int index = _lngvm.LngItemVM.FindIndex(x => x.KeyCode == _lngFrom.ToLower());
                int index = _lngvm.LngItemVM.FindIndex(x => x.KeyCode == _lngFrom);
                this.sltLngFrom.SelectedIndex = index;
                _lngvm.ListFromSelectedIndex = index;
                this.sltLngFrom.IsEnabled = false;

                _lngToItem = _lngvm.LngItemVM.ToList();
                //foreach (Language lng in _lngvm.LngItemVM)
                //{
                //    _lngToItem.Add(lng.GetCopy());
                //}

                foreach (Translate transl in WordViewModel.WordList[indexWord].Transl)
                {
                    //_lngToItem.RemoveAll(x => x.KeyCode == transl.LngTo.ToLower());
                    _lngToItem.RemoveAll(x => x.KeyCode == transl.LngTo);
                }
               // _lngToItem.RemoveAll( x => x.KeyCode == _lngFrom.ToLower());
                _lngToItem.RemoveAll(x => x.KeyCode == _lngFrom);
                this.sltLngTo.ItemsSource = _lngToItem;

                _lngTo = "vi";
                index = _lngToItem.FindIndex(x => x.KeyCode == _lngTo);
                if (index != -1)
                    this.sltLngTo.SelectedIndex = index;
                this.sltLngTo.SelectedIndex = 0;
               
            }
            else
            {
                    _lngFrom = "en";
                    int index = _lngvm.LngItemVM.FindIndex(x => x.KeyCode == _lngFrom);
                    this.sltLngFrom.SelectedIndex = index;
                    _lngvm.ListFromSelectedIndex = index;
            }

            if (_name != string.Empty)
            {
                txtNewWord.Text = _name;
                txtNewWord.IsReadOnly = true;
                
            }
        }

        private void sltLngFrom_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _lngvm.ListFromSelectedIndex = sltLngFrom.SelectedIndex;

            if (_lngToItem != null)
            {
                this.sltLngTo.ItemsSource = _lngToItem;
            }
            else
            {
                this.sltLngTo.ItemsSource = _lngvm.LngToItem;

                _lngTo = "vi";
                int index = _lngvm.LngToItem.FindIndex(x => x.KeyCode == _lngTo);
                if (index != -1)
                    this.sltLngTo.SelectedIndex = index;
                
            }
            
        }

        private void sltLngTo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           
            
        }

        private void btnAddNewWord_Click(object sender, EventArgs e)
        {
            if (txtNewWord.Text.Length > 0)
            {
                _settings.Save();
                Language curItemFrom = (Language)sltLngFrom.SelectedItem;
                Language curItemTo = (Language)sltLngTo.SelectedItem;

                string strLngFrom ;
                string strLngTo;

                if (_fromOrTo == 1)
                {
                     strLngFrom = curItemFrom.KeyCode;
                     strLngTo = curItemTo.KeyCode;
                }
                else
                {
                     strLngTo = curItemFrom.KeyCode;
                     strLngFrom = curItemTo.KeyCode;
                }
                

                Translate trans = new Translate(strLngTo,"",txtNote.Text);
                List<Translate> translate = new List<Translate>();
                translate.Add(trans);
                Word newWord = new Word(txtNewWord.Text, strLngFrom, "", new ObservableCollection<string>(), translate);
                WordViewModel.AddWordToList(newWord);

                var lastPage = NavigationService.BackStack.FirstOrDefault();
                if (lastPage != null && !lastPage.Source.ToString().Equals("/MainPage.xaml"))
                {
                    NavigationService.RemoveBackEntry();
                    NavigationService.Navigate(new Uri(lastPage.Source.ToString(), UriKind.Relative));
                }
                else
                {
                    NavigationService.GoBack();
                }

            }
            
        }

        private void txtFromTo_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (!PhoneApplicationService.Current.State.ContainsKey("IndexWord"))
            {
                if (_fromOrTo == 1)
                {
                    this.txtFromTo.Text = "<--";
                    _fromOrTo = -1;
                }
                else
                {
                    _fromOrTo = 1;
                    this.txtFromTo.Text = "-->";

                }
            }

            
        }

        

    }
}