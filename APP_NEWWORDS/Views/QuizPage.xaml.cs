﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace APP_NEWWORDS.Views
{
    public partial class QuizPage : PhoneApplicationPage
    {
        public QuizPage()
        {
            InitializeComponent();
        }

        private void btnAnswer_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/QuizLinkWordPage.xaml", UriKind.Relative));
        }

        private void btnGuess_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/QuizGuessWordPage.xaml", UriKind.Relative));
        }

        private void PhoneApplicationPage_BackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
        {
            NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
        }
    }
}