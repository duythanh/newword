﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media;
using System.Collections.ObjectModel;
using APP_NEWWORDS.Models;
using System.Threading;
using APP_NEWWORDS.ViewModels;
using System.Diagnostics;
using Microsoft.Phone.Net.NetworkInformation;
using System.Windows.Media.Imaging;
using Microsoft.Phone.Controls.Primitives;

namespace APP_NEWWORDS.Views
{
    public partial class NewWordDetails : PhoneApplicationPage
    {
        int gIndex = -1;

        List<string> BangMau = new List<string>();
        WebBrowserHelper _wbHelper;
        public NewWordDetails()
        {
            InitializeComponent();

            BangMau.Add("#FFC47F7F");
            BangMau.Add("#FFE4CB88");
            BangMau.Add("#FFA7D666");
            BangMau.Add("#FFB7C4DA");
            BangMau.Add("#FF4D9BC4");

            if (PhoneApplicationService.Current.State.ContainsKey("IndexWord"))
                gIndex = (int)PhoneApplicationService.Current.State["IndexWord"];

            //_wbHelper = new WebBrowserHelper(this.wbEnDes);
            //_wbHelper.ScrollDisabled = true;
            this.Loaded += NewWordDetails_Loaded;
        }

        private int ConvertToColor(int pIndex)
        {
            return pIndex % BangMau.Count;
        }

        private void NewWordDetails_Loaded(object sender, RoutedEventArgs e)
        {
            // _loadAgain = true;
            init();
            TryUpdateInterface();
        }

        public void init()
        {
            if (gIndex != -1)
            {
                for (int i = 0; i < WordViewModel.WordList[gIndex].Transl.Count; i++)
                {
                    WordViewModel.WordList[gIndex].Transl[i].Brush = BangMau[ConvertToColor(i)];
                }

                tbNewword.Text = WordViewModel.WordList[gIndex].Name + " (" + WordViewModel.WordList[gIndex].LngFrom + ")";
                this.lstNghia.ItemsSource = WordViewModel.WordList[gIndex].Transl;


            }
        }

        private void TryUpdateInterface()
        {
            Thread t = new Thread(ToDoImageAndEnDes);
            t.Start();
        }

        private void ToDoImageAndEnDes()
        {

            if (NetworkInterface.NetworkInterfaceType != NetworkInterfaceType.None)
            {
                Deployment.Current.Dispatcher.BeginInvoke(() => { lstImages.ItemsSource = WordViewModel.WordList[gIndex].Images; });


                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    {
                        string head = @"<html>
<body>";



                        string tail = @"</body>
</html>";
                        string tempEnDescription;
                        if (WordViewModel.WordList[gIndex].LngFrom.Equals("en"))
                        {
                            tempEnDescription = (WordViewModel.WordList[gIndex].EnDescription.Equals("")) ? "<strong>Sorry, couldn't find what you're looking</strong>" : WordViewModel.WordList[gIndex].EnDescription;
                        }
                        else
                        {
                            tempEnDescription = "<strong>Description is only available for English words</strong>";
                        }

                        string example =
                            "<html> <head> <script> function SendDataToPhoneApp() { window.external.notify('100'); } function sayHello(sayWhat) { return sayWhat; } </script> </head> <body> Hello </body> </html>";
                        var htmlScript = "<script>" +
                                         "function getDocHeight() { " +
                                         "return document.getElementById('pageWrapper').offsetHeight;}" +
                                         "function SendDataToPhoneApp() {" +
                                         "window.external.notify('' + getDocHeight());" +
                                         "}</script>";
                        string meta = "<meta name=\"viewport\" content=\"width=320,user-scalable=no\" />";
                        string tempHTML = "<html><head>" + htmlScript +
                            "</head>" + "<body style=\"margin:0px;padding:0px;width:100%;\" " +
                            "onLoad=\"SendDataToPhoneApp()\"> <div id=\"pageWrapper\" style=\"width:100%;\">" + tempEnDescription + "</div></body></html>";
                        wbEnDes.NavigateToString(tempHTML);
                        wbEnDes.IsScriptEnabled = true;
                        wbEnDes.ScriptNotify +=
                          new EventHandler<NotifyEventArgs>(WBHeight_ScriptNotify);
                        //wbEnDes.InvokeScript("SendDataToPhoneApp");
                    }


                });

            }
            else
            {
                if (WordViewModel.WordList[gIndex].EnDescription.Length > 0)
                {
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        {
                            string head = @"<html>
    <body>";



                            string tail = @"</body>
    </html>";
                            string tempEnDescription;
                            if (WordViewModel.WordList[gIndex].LngFrom.Equals("en"))
                            {
                                tempEnDescription = (WordViewModel.WordList[gIndex].EnDescription.Equals("")) ? "<strong>Sorry, couldn't find what you're looking</strong>" : WordViewModel.WordList[gIndex].EnDescription;
                            }
                            else
                            {
                                tempEnDescription = "<strong>Description is only available for English words</strong>";
                            }

                            string example =
                                "<html> <head> <script> function SendDataToPhoneApp() { window.external.notify('100'); } function sayHello(sayWhat) { return sayWhat; } </script> </head> <body> Hello </body> </html>";
                            var htmlScript = "<script>" +
                                             "function getDocHeight() { " +
                                             "return document.getElementById('pageWrapper').offsetHeight;}" +
                                             "function SendDataToPhoneApp() {" +
                                             "window.external.notify('' + getDocHeight());" +
                                             "}</script>";
                            string meta = "<meta name=\"viewport\" content=\"width=320,user-scalable=no\" />";
                            string tempHTML = "<html><head>" + htmlScript +
                                "</head>" + "<body style=\"margin:0px;padding:0px;width:100%;\" " +
                                "onLoad=\"SendDataToPhoneApp()\"> <div id=\"pageWrapper\" style=\"width:100%;\">" + tempEnDescription + "</div></body></html>";
                            wbEnDes.NavigateToString(tempHTML);
                            wbEnDes.IsScriptEnabled = true;
                            wbEnDes.ScriptNotify +=
                              new EventHandler<NotifyEventArgs>(WBHeight_ScriptNotify);
                            //wbEnDes.InvokeScript("SendDataToPhoneApp");
                        }


                    });
                }
                else
                {
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        {
                            string head = @"<html>
    <body>";



                            string tail = @"</body>
    </html>";
                            string tempEnDescription = "<strong>You are offline !</strong>";

                            string example =
                                "<html> <head> <script> function SendDataToPhoneApp() { window.external.notify('100'); } function sayHello(sayWhat) { return sayWhat; } </script> </head> <body> Hello </body> </html>";
                            var htmlScript = "<script>" +
                                             "function getDocHeight() { " +
                                             "return document.getElementById('pageWrapper').offsetHeight;}" +
                                             "function SendDataToPhoneApp() {" +
                                             "window.external.notify('' + getDocHeight());" +
                                             "}</script>";
                            string meta = "<meta name=\"viewport\" content=\"width=320,user-scalable=no\" />";
                            string tempHTML = "<html><head>" + htmlScript +
                                "</head>" + "<body style=\"margin:0px;padding:0px;width:100%;\" " +
                                "onLoad=\"SendDataToPhoneApp()\"> <div id=\"pageWrapper\" style=\"width:100%;\">" + tempEnDescription + "</div></body></html>";
                            wbEnDes.NavigateToString(tempHTML);
                            wbEnDes.IsScriptEnabled = true;
                            wbEnDes.ScriptNotify +=
                              new EventHandler<NotifyEventArgs>(WBHeight_ScriptNotify);
                            //wbEnDes.InvokeScript("SendDataToPhoneApp");
                        }
                    });
                    
                }
            }

        }

        private void WBHeight_ScriptNotify(object sender, NotifyEventArgs e)
        {
            // The browser is zooming the text so we need to
            // reduce the pixel size by the zoom level... 
            // Which is about 0.50
            wbEnDes.Height = Convert.ToDouble(e.Value)*0.4;
        }

        private void ClearImageCache()
        {
            List<Image> listImage = new List<Image>();
            GetImageChildByName(this.lstImages, "imgPics", listImage);

            foreach (Image img in listImage)
            {
                var bitmap = img.Source as BitmapImage;
                bitmap.UriSource = null;
                img.Source = null;

            }
        }

        private void GetImageChildByName(DependencyObject parent, string name, List<Image> listImage)
        {
            var count = VisualTreeHelper.GetChildrenCount(parent);
            if (count > 0)
            {
                for (int i = 0; i < count; i++)
                {
                    var child = VisualTreeHelper.GetChild(parent, i);
                    if (child is Image)
                    {
                        Image targetItem = (Image)child;
                        if (targetItem.Name == name)
                        {

                            listImage.Add(targetItem);
                        }
                    }
                    else
                    {
                        GetImageChildByName(child, name, listImage);
                    }
                }
            }
        }

        private void DisplayWordList()
        {
            try
            {
                this.lstNghia.ItemsSource = null;
                this.lstNghia.ItemsSource = WordViewModel.WordList[gIndex].Transl;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {

            MenuItem cntMenu = (MenuItem)sender;
            int tIndex = -1;
            if (cntMenu != null)
            {
                object item = cntMenu.DataContext;


                if (item != null)
                {
                    tIndex = this.lstNghia.Items.IndexOf(item);
                }
            }
            if (tIndex != -1)
            {
                WordViewModel.WordList[gIndex].Transl.RemoveAt(tIndex);
                WordViewModel.WriteWordList();
                if (WordViewModel.WordList[gIndex].Transl.Count() != 0)
                {
                    DisplayWordList();
                }
                else
                {
                    WordViewModel.WordList.RemoveAt(gIndex);
                    WordViewModel.WriteWordList();
                    PhoneApplicationService.Current.State.Remove("IndexWord");
                    ClearImageCache();
                    NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
                }

            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            ClearImageCache();
            NavigationService.Navigate(new Uri("/Views/AddWordPage.xaml", UriKind.Relative));
        }

        private void PhoneApplicationPage_BackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
        {
            PhoneApplicationService.Current.State.Remove("IndexWord");
            ClearImageCache();
            NavigationService.Navigate(new Uri("/Mainpage.xaml", UriKind.Relative));
        }


        private void TxtNote_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox txtNote = sender as TextBox;
            WordViewModel.WriteWordList();
        }
    }
}