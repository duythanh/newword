﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace APP_NEWWORDS.Models
{
    public class Language
    {
        public string Lang { get; set; }
        public string KeyCode { get; set; }

        public Language()
        {
        }

        public Language(string pLanguage, string pKeyCode)
        {
            Lang = pLanguage;
            KeyCode = pKeyCode;
        }

        public Language GetCopy()
        {
            return this.MemberwiseClone() as Language;
        }
    }
}
