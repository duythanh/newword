﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Microsoft.Phone.Net.NetworkInformation;
using System.Windows;
using Microsoft.Phone.Tasks;

namespace APP_NEWWORDS.Models
{
    public class StaticMethod
    {
        private static async Task<string> getHTML(string uri)
        {
            /*
             * Console.WriteLine("=== HttpClient ==");
        var client = new HttpClient();
        var request = new HttpRequestMessage(HttpMethod.Get, new Uri("http://www.google.com"));
        Console.WriteLine("HttpClient requesting...");
        var response = await client.SendAsync(request);
        Console.WriteLine(response.Content.ReadAsStringAsync().Result.Substring(0,100));
        Console.WriteLine("HttpClient done");*/


            uri += "&ignored=" + DateTime.UtcNow.Ticks;
            var tcs = new TaskCompletionSource<string>();
            var wc = new System.Net.WebClient();
            //TODO: Check for network connection
            if (NetworkInterface.GetIsNetworkAvailable())
            {
                wc.DownloadStringCompleted += (s, e) =>
                {
                    if (e.Error != null)
                    {
                        //MessageBox.Show("No Internet connection detected.");
                        tcs.TrySetException(e.Error);
                    }

                    else if (e.Cancelled)
                        tcs.TrySetCanceled();
                    else
                        tcs.TrySetResult(e.Result);
                };
                wc.DownloadStringAsync(new Uri(uri));
                return await tcs.Task;
            }
            else
            {
                if (MessageBox.Show("No Internet connection detected. Tap OK to switch on 3G/Wifi to use app. Tap Cancel to quit app"
                    , "No Network", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                {
                    ConnectionSettingsTask con = new ConnectionSettingsTask();
                    con.ConnectionSettingsType = ConnectionSettingsType.WiFi;
                    con.Show();
                }
                else
                {
                    Quit();
                }
                return null;
            }

        }

        private class QuitException : Exception { }

        public static void Quit()
        {
            throw new QuitException();
        }


        public static async Task<Word.ImagesRootObject> getGoogleImages(string word)
        {
            //http://ajax.googleapis.com/ajax/services/search/images?v=1.0&rsz=large&start=0&q=%22lead%22

            string temp = "http://ajax.googleapis.com/ajax/services/search/images?v=1.0&rsz=large&start=0&q=\"" + word + "\"";

            string result = await getHTML(temp);
            JObject newJObject = JObject.Parse(result);
            var ret = newJObject.ToObject<Word.ImagesRootObject>();
            return ret;

        }

        public static async Task<Word.ENDescriptionRootObject> getENDescription(string word)
        {
            //http://blachan.com/shahi/define.php?words=hey

            string temp = "http://blachan.com/shahi/define.php?words=" + word;

            string result = await getHTML(temp);
            JObject newJObject = JObject.Parse(result);
            var ret = newJObject.ToObject<Word.ENDescriptionRootObject>();
            return ret;

        }
    }
}
