﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace APP_NEWWORDS.Models
{
    public class Translate : ModelBase
    {
        //fields
        private string _lngTo;
        private string _description;
        private string _note;
        private string _brush;

        //properties
        public string LngTo
        {
            get { return _lngTo; }
            set { _lngTo = value; NotifyPropertyChanged("LngTo"); }
        }
        public string Description
        {
            get { return _description; }
            set 
            { 
                _description = value;
                NotifyPropertyChanged("Description");
            }
        }
        public string Note
        {
            get { return _note; }
            set { _note = value; NotifyPropertyChanged("Note"); }
        }
        public string Brush
        {
            get { return _brush; }
            set { _brush = value; NotifyPropertyChanged("Brush"); }
        }
        
        //contructor
        public Translate()
        {
            LngTo = "";
            Description = "";
            Note = "";
        }

        public Translate(string pLngTo, string pDescription, string pNote)
        {
            LngTo = pLngTo;
            Description = pDescription;
            Note = pNote;
        }
    }
}
