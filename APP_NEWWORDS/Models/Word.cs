﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APP_NEWWORDS.Models
{
    public class Word:ModelBase
    {
        //fields
        private string _name;
        private string _lngFrom;
        private string _enDescription;
        private ObservableCollection<string> _images;

        public ObservableCollection<string> Images
        {
            get { return _images; }
            set { 
                    _images = value;
                  //  NotifyPropertyChanged("Images");
                }
        }

        private List<Translate> _transl;
        private string _brush;

        //properties
        public string Name
        {
            get { return _name; }
            set { 
                _name = value;
                NotifyPropertyChanged("Name");
            }
        }
        public string LngFrom
        {
            get { return _lngFrom; }
            set { 
                _lngFrom = value;
               // NotifyPropertyChanged("LngFrom");
            }
        }
        public string EnDescription
        {
            get { return _enDescription; }
            set { 
                _enDescription = value;
                NotifyPropertyChanged("EnDescription");
            }
        }

       

        public List<Translate> Transl
        {
            get { return _transl; }
            set { 
                _transl = value;
                NotifyPropertyChanged("Transl");
            }
        }
        public string Brush
        {
            get { return _brush; }
            set { _brush = value; }
        }

        //Contructor
        public Word()
        {
            Name = "";
            LngFrom = "";
            Transl = new List<Translate>();
            Images = new ObservableCollection<string>();
        }

        public Word(string pName, string pLngFrom, string pEnDescription, ObservableCollection<string> pImages, List<Translate> pTrasl)
        {
            Name = pName;
            LngFrom = pLngFrom;
            EnDescription = pEnDescription;
            Images = pImages;
            Transl = pTrasl;
        }

        public Word GetCopy()
        {
            return this.MemberwiseClone() as Word;
        }

        public class Result
        {
            public string GsearchResultClass { get; set; }
            public string width { get; set; }
            public string height { get; set; }
            public string imageId { get; set; }
            public string tbWidth { get; set; }
            public string tbHeight { get; set; }
            public string unescapedUrl { get; set; }
            public string url { get; set; }
            public string visibleUrl { get; set; }
            public string title { get; set; }
            public string titleNoFormatting { get; set; }
            public string originalContextUrl { get; set; }
            public string content { get; set; }
            public string contentNoFormatting { get; set; }
            public string tbUrl { get; set; }
        }

        public class Page
        {
            public string start { get; set; }
            public int label { get; set; }
        }

        public class Cursor
        {
            public string resultCount { get; set; }
            public List<Page> pages { get; set; }
            public string estimatedResultCount { get; set; }
            public int currentPageIndex { get; set; }
            public string moreResultsUrl { get; set; }
            public string searchResultTime { get; set; }
        }

        public class ResponseData
        {
            public List<Result> results { get; set; }
            public Cursor cursor { get; set; }
        }

        public class ImagesRootObject
        {
            public ResponseData responseData { get; set; }
            public object responseDetails { get; set; }
            public int responseStatus { get; set; }
        }

        public class ENDescriptionRootObject
        {
            public string stat { get; set; }
            public string word { get; set; }
            public string def { get; set; }
            public string see { get; set; }
        }

    }

   
}
