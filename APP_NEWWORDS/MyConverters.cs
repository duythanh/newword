﻿using APP_NEWWORDS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace APP_NEWWORDS
{
    public class TextToUpper : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string str = (string)value;
            return str.ToUpper();
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ResizeImage : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            if (value != null && value.GetType() == typeof(string))
            {
                string str = (string)value;
                var bmp = new BitmapImage();

                bmp.DecodePixelWidth = 150;
                bmp.DecodePixelHeight = 150;
                bmp.UriSource = new Uri(str);
                return bmp;
            }
            return null;     
            
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class TranslToDes : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            if (value != null && value.GetType() == typeof(List<Translate>))
            {
                List<Translate> transl = value as List<Translate>;
                if(transl.Count > 0)
                {
                    return transl[0].Description;
                }
                
            }
            return null;     
            
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class TranslToLngTo : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            if (value != null && value.GetType() == typeof(List<Translate>))
            {
                List<Translate> transl = value as List<Translate>;
                if (transl.Count > 0)
                {
                    return transl[0].LngTo;
                }

            }
            return null;

        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class TranslToNote : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            if (value != null && value.GetType() == typeof(List<Translate>))
            {
                List<Translate> transl = value as List<Translate>;
                if (transl.Count > 0)
                {
                    return transl[0].Note;
                }

            }
            return null;

        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
