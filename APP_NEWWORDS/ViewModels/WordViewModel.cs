﻿using APP_NEWWORDS.Models;
using HtmlAgilityPack;
using Microsoft.Phone.Net.NetworkInformation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace APP_NEWWORDS.ViewModels
{
    public class WordViewModel
    {
        private static ObservableCollection<Word> _wordList;
        private static SerializableDictionary<string, Word> _wordQueue;
        private static bool isStop = true; // dung de xem co thread  DoTranslateWordInQueue nao dang chay chua
        public volatile static bool isContinue = true; // dung de xu ly tuan tu tu trong Queue
        private static object _lock;

        public static SerializableDictionary<string, Word> WordQueue
        {
            get { return WordViewModel._wordQueue; }
            set { WordViewModel._wordQueue = value; }
        }


        public static ObservableCollection<Word> WordList
        {
            get { return WordViewModel._wordList; }
            set { WordViewModel._wordList = value; }
        }

        private static string FILE_NAME_WORD_LIST = "newwords_wordlist.xml";
        private static string FILE_NAME_WORD_QUEUE = "newwords_wordqueue.xml";

        public static void ReadWordList()
        {
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(FILE_NAME_WORD_LIST, FileMode.Open))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(ObservableCollection<Word>));
                        WordList = (ObservableCollection<Word>)serializer.Deserialize(stream);
                        stream.Close();
                    }
                }
            }
            catch
            {
                WordList = new ObservableCollection<Word>();
            }
        }

        public static void WriteWordList()
        {
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(FILE_NAME_WORD_LIST, FileMode.Create))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(ObservableCollection<Word>));
                    using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                    {
                        serializer.Serialize(xmlWriter, WordList);
                    }
                }
            }
        }

        public static void ReadWordQueue()
        {
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(FILE_NAME_WORD_QUEUE, FileMode.Open))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(SerializableDictionary<string, Word>));
                        WordQueue = (SerializableDictionary<string, Word>)serializer.Deserialize(stream);
                        foreach(var key in WordQueue.Keys.ToList()){
                            var keysplit = key.Split('-');
                            var wordname = keysplit[0];
                            var lngFr = keysplit[1];
                            WordQueue[key] = WordList.Where(x => x.Name.Equals(wordname) && x.LngFrom.Equals(lngFr)).ToList().First();    
                        }
                        stream.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                if(WordViewModel.WordQueue == null)
                    WordViewModel.WordQueue = new SerializableDictionary<string,Word>();
            }


        }

        public static void WriteWordQueue()
        {
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(FILE_NAME_WORD_QUEUE, FileMode.Create))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(SerializableDictionary<string, Word>));
                    using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                    {
                        serializer.Serialize(xmlWriter, WordQueue);
                    }
                }
            }
        }

        public static Word GetWordByName(string name)
        {
            return WordList.FirstOrDefault(w => w.Name == name);
        }

        public static void AddWordToList(Word pWord)
        {
            Word tmp = GetWordByName(pWord.Name);
            if (tmp == null)
            {

                WordList.Insert(0, pWord);
                WriteWordList();
                AddWordToQueue(pWord);

            }
            else if (tmp.Transl.FirstOrDefault(x => x.LngTo == pWord.Transl[0].LngTo) == null)
            {
                tmp.Transl.Insert(0, pWord.Transl[0]);
                AddWordToQueue(pWord);
            }

        }

        private static void AddWordToQueue(Word pWord)
        {
            WordQueue.Add(pWord.Name + "-" + pWord.LngFrom + "-" + pWord.Transl[0].LngTo, pWord);
            WriteWordQueue();
            TryDoTranslateWordInQueue();
        }

        public async static void AddEnDescriptionToWord(Word pWord)
        {
            try
            {
                var ENDescription = await StaticMethod.getENDescription(pWord.Name);
                pWord.EnDescription = Environment.NewLine + Regex.Replace(ENDescription.def, "</?(a|A).*?>", "") + Environment.NewLine;

            }
            catch (Exception ex)
            {
            }

        }

        public async static void AddImageToWord(Word pWord)
        {
            try
            {
                var tImages = await StaticMethod.getGoogleImages(pWord.Name);
                lock (pWord.Images)
                {

                  //  pWord.Images = new ObservableCollection<string>();
                    for (int i = 0; i < tImages.responseData.results.Count; i++)
                    {
                        pWord.Images.Add(tImages.responseData.results[i].url);
                    }

                    if (pWord.Images.Count == 0)
                        pWord.Images = null;
                }

            }
            catch (Exception ex)
            {
            }
        }

        public static void TryDoTranslateWordInQueue()
        {
            if (isStop)
            {
                isStop = false;
                Thread t = new Thread(DoTranslateWordInQueue);
                t.Start();
            }
        }
        private static void DoTranslateWordInQueue()
        {
            if(NetworkInterface.NetworkInterfaceType != NetworkInterfaceType.None)
                foreach (var temp in WordViewModel.WordQueue) 
            {
                try
                {
                    ProcessWordData(temp.Value);
                }
                catch
                {                    
                    if (WordViewModel.WordQueue.Count > 0)
                    {
                        WordViewModel.WordQueue.Remove(WordViewModel.WordQueue.Last().Key);
                        WordViewModel.WriteWordQueue();
                        
                    }
                }
            }

            isStop = true;
        }

        public static void ProcessWordData(Word word)
        {
            //Word word = WordViewModel.WordQueue.First().Value;
            if (word.LngFrom == "en")
            {
                if (word.EnDescription == string.Empty)
                    WordViewModel.AddEnDescriptionToWord(word);
            }
            if (word.Images != null && word.Images.Count == 0)
                AddImageToWord(word);

            BingTransl bngTransl = new BingTransl(word);
        }
    }
}
