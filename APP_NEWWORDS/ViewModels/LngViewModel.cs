﻿using APP_NEWWORDS.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APP_NEWWORDS.ViewModels
{
    public class LngViewModel : ViewModeBase
    {

        private string[] cultureNames =
        {
            "ar", "bg", "ca", "cs", "da", "nl", "en", "et", "fa", "fi", "fr", "de", "el",
            "he", "hi", "hu", "id", "it", "ja", "ko", "lv", "lt", "ms", "no", "pl", "pt", "ro", "ru", "sk", "sl", "es",
            "sv", "th", "tr", "uk", "ur", "vi"
        };

        private string[] cultureFullNames =
        {
            "Arabic", "Bulgarian", "Catalan", "Czech", "Danish", "Dutch", "English", "Estonian", "Persian", "Finnish",
            "French", "German", "Greek", "Hebrew", "Hindi", "Hungarian", "Indonesian", "Italian", "Japanese", "Korean",
            "Latvian", "Lithuanian", "Malay", "Norwegian", "Polish", "Portuguese", "Romanian", "Russian", "Slovak",
            "Slovenian", "Spanish", "Swedish", "Thai", "Turkish", "Ukrainian", "Urdu", "Vietnamese"
        };

        private static List<Language> _lngItemVM;

        public List<Language> LngItemVM
        {
            get { return _lngItemVM; }
            set
            {
                if (_lngItemVM != value)
                {
                    _lngItemVM = value;
                }
                NotifyPropertyChanged("LngItemVM");
            }
        }

        private List<Language> _lngToItem;

        public List<Language> LngToItem
        {
            get
            {
                _lngToItem = _lngItemVM.ToList();
                _lngToItem.RemoveAt(ListFromSelectedIndex);
                return _lngToItem;
            }
        }

        private int _listFromSelectedIndex = -1;

        public int ListFromSelectedIndex
        {
            get { return _listFromSelectedIndex; }
            set
            {
                if (_listFromSelectedIndex != value)
                {
                    _listFromSelectedIndex = value;
                }
                NotifyPropertyChanged("ListFromSelectedIndex");
            }
        }


        public LngViewModel()
        {
            if (_lngItemVM == null)
            {
                _lngItemVM = new List<Language>();
                getCultureNames();
            }
        }

        private void getCultureNames()
        {
            for (int i = 0; i < cultureNames.Length; i++)
            {
                _lngItemVM.Add(new Language(cultureFullNames[i], cultureNames[i]));
            }
        }

        private CultureInfo CreateCultureInfo(string cultureName)
        {
            try
            {
                return new CultureInfo(cultureName);
            }
            catch (Exception)
            {
                return null;
            }
        }

    }
}
