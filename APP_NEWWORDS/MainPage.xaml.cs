﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using APP_NEWWORDS.Resources;
using System.IO.IsolatedStorage;
using System.IO;
using System.Xml.Serialization;
using APP_NEWWORDS.Models;
using APP_NEWWORDS.ViewModels;
using Microsoft.Phone.Net.NetworkInformation;
using System.Diagnostics;
using System.Threading;

namespace APP_NEWWORDS
{
    public partial class MainPage : PhoneApplicationPage
    {
        #region
        List<string> BangMau;
        //1. Create color
        #region
        private int convertToColor(int pIndex)
        {
            return pIndex % BangMau.Count;
        }
        private void CreateColorItemsListBox()
        {
            BangMau = new List<string>();
            BangMau.Add("#FFC47F7F");
            BangMau.Add("#FFE4CB88");
            BangMau.Add("#FFA7D666");
            BangMau.Add("#FFB7C4DA");
            BangMau.Add("#FF4D9BC4");
        }
        #endregion

        #endregion
      
        public MainPage()
        {
            InitializeComponent();
            CreateColorItemsListBox();           
            Loaded += MainPage_Loaded;

        }


        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (e.NavigationMode == NavigationMode.New)
            {
                WordViewModel.TryDoTranslateWordInQueue();
            }
        }
        private void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            
            DisplayWordList();
            
            while (NavigationService.CanGoBack)
            {
                NavigationService.RemoveBackEntry();
            }

            
        }

        private void DisplayWordList()
        {
            //try
            //{
            //    for (int i = 0; i < WordViewModel.WordList.Count; i++)
            //    {
            //        WordViewModel.WordList[i].Brush = BangMau[convertToColor(i)];
            //    }
            //    this.ltbWordList.ItemsSource = null;
            //    this.ltbWordList.ItemsSource = WordViewModel.WordList;
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.ToString());
            //}

            
            

            if (WordViewModel.WordList.Count < 1)
            {
                this.txtNoWord.Visibility = System.Windows.Visibility.Visible;
                this.txtNoWord.Text = "Please tap the + button to add a new word!";
            }
            else
            {
                this.txtNoWord.Visibility = System.Windows.Visibility.Collapsed;
                for (int i = 0; i < WordViewModel.WordList.Count; i++)
                {
                    WordViewModel.WordList[i].Brush = BangMau[convertToColor(i)];
                }
                this.ltbWordList.ItemsSource = null;
                this.ltbWordList.ItemsSource = WordViewModel.WordList;
            }
        }

        private void lstListNewWord_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int tIndex = ltbWordList.SelectedIndex;
            if (tIndex != -1)
            {
                PhoneApplicationService.Current.State["IndexWord"] = tIndex;
                NavigationService.Navigate(new Uri("/Views/NewWordDetailsPage.xaml", UriKind.Relative));
            }
        }

        private void Add_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/AddWordPage.xaml", UriKind.Relative));
        }


        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            MenuItem cntMenu = (MenuItem)sender;
             int tIndex = -1;
             if (cntMenu != null)
                {
                    object item = cntMenu.DataContext;
                    if (item != null)
                    {
                        tIndex = this.ltbWordList.Items.IndexOf(item);
                    }
                }

             if (tIndex != -1)
            {
                WordViewModel.WordList.RemoveAt(tIndex);
                 
                WordViewModel.WriteWordList();
                DisplayWordList();
            }
        }

        private void Quiz_Click(object sender, EventArgs e)
        {
            if (WordViewModel.WordList.Count > 0)
            {
                NavigationService.Navigate(new Uri("/Views/QuizPage.xaml", UriKind.Relative));
            }
            else
            {
                MessageBox.Show("Have no words to play games");
            }
        } 
    }
}