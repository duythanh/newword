﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using APP_NEWWORDS.Models;
using APP_NEWWORDS.ViewModels;
using System.Threading;
using System.Diagnostics;


namespace APP_NEWWORDS
{
    public class BingTransl
    {
        public ManualResetEvent mrse = new ManualResetEvent(false);
        private string strLngFrom;
        private string strLngTo;
        private string strTextToTranslate;
        private string clientID = "SHTPFlashCard";
        private string clientSecret = "txJeycwvaKAYlyFpshN8TuXNuVyboPASL9/qG8ED0qY=";
        Word newWord = null;

        public BingTransl(Word pWord)
        {
            newWord = pWord;
            this.strLngFrom = pWord.LngFrom;
            this.strLngTo = pWord.Transl[0].LngTo;
            this.strTextToTranslate = pWord.Name;
          //  WordModel.WordList.Add(newWord);
            String strTranslatorAccessURI =
                 "https://datamarket.accesscontrol.windows.net/v2/OAuth2-13";
            System.Net.WebRequest req = System.Net.WebRequest.Create(strTranslatorAccessURI);
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";
            // Important to note -- the call back here isn't that the request was processed by the server
            // but just that the request is ready for you to do stuff to it (like writing the details)
            // and then post it to the server.
            IAsyncResult writeRequestStreamCallback =
              (IAsyncResult)req.BeginGetRequestStream(new AsyncCallback(RequestStreamReady), req);
        }

        private void RequestStreamReady(IAsyncResult ar)
        {
            // STEP 2: The request stream is ready. Write the request into the POST stream
            //string clientID = "<<Your Client ID>>";
            //string clientSecret = "<<Your Client Secret>>";

            String strRequestDetails = string.Format("grant_type=client_credentials&client_id={0}&client_secret={1}&scope=http://api.microsofttranslator.com", HttpUtility.UrlEncode(clientID), HttpUtility.UrlEncode(clientSecret));

            // note, this isn't a new request -- the original was passed to beginrequeststream, so we're pulling a reference to it back out. It's the same request

            System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)ar.AsyncState;
            // now that we have the working request, write the request details into it
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(strRequestDetails);
            System.IO.Stream postStream = request.EndGetRequestStream(ar);
            postStream.Write(bytes, 0, bytes.Length);
            postStream.Close();
            // now that the request is good to go, let's post it to the server
            // and get the response. When done, the async callback will call the
            // GetResponseCallback function
            request.BeginGetResponse(new AsyncCallback(GetResponseCallback), request);
        }

        private void GetResponseCallback(IAsyncResult ar)
        {

            try
            {
                // STEP 3: Process the response callback to get the token
                // we'll then use that token to call the translator service
                // Pull the request out of the IAsynch result
                HttpWebRequest request = (HttpWebRequest)ar.AsyncState;
                // The request now has the response details in it (because we've called back having gotten the response
                HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(ar);
                // Using JSON we'll pull the response details out, and load it into an AdmAccess token class (defined earlier in this module)
                // IMPORTANT (and vague) -- despite the name, in Windows Phone, this is in the System.ServiceModel.Web library,
                // and not the System.Runtime.Serialization one -- so you will need to have a reference to System.ServiceModel.Web

                System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new
                System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(AdmAccessToken));
                AdmAccessToken token =
                  (AdmAccessToken)serializer.ReadObject(response.GetResponseStream());

                string uri = "http://api.microsofttranslator.com/v2/Http.svc/Translate?text=" + System.Net.HttpUtility.UrlEncode(strTextToTranslate) + "&from=" + strLngFrom + "&to=" + strLngTo;
                System.Net.WebRequest translationWebRequest = System.Net.HttpWebRequest.Create(uri);
                // The authorization header needs to be "Bearer" + " " + the access token
                string headerValue = "Bearer " + token.access_token;
                translationWebRequest.Headers["Authorization"] = headerValue;
                // And now we call the service. When the translation is complete, we'll get the callback
                IAsyncResult writeRequestStreamCallback = (IAsyncResult)translationWebRequest.BeginGetResponse(new AsyncCallback(TranslationReady), translationWebRequest);
            }
            catch (Exception ex)
            {
               // Deployment.Current.Dispatcher.BeginInvoke(() => MessageBox.Show("You are offline 1 : " + ex.Message));
            }
        }

        private void TranslationReady(IAsyncResult ar)
        {
            try
            {
                // STEP 4: Process the translation
                // Get the request details
                HttpWebRequest request = (HttpWebRequest)ar.AsyncState;

                // Get the response details
                HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(ar);
                System.IO.Stream streamResponse = response.GetResponseStream();
                System.IO.StreamReader streamRead = new System.IO.StreamReader(streamResponse);
                string responseString = streamRead.ReadToEnd();
                // Translator returns XML, so load it into an XDocument
                // Note -- you need to add a reference to the System.Linq.XML namespace
                System.Xml.Linq.XDocument xTranslation =
                    System.Xml.Linq.XDocument.Parse(responseString);
                string strTest = xTranslation.Root.FirstNode.ToString();
                if (strTest != string.Empty)
                {
                    if (this.newWord != null)
                    {
                        Deployment.Current.Dispatcher.BeginInvoke(() =>
                        {                            
                            for (int i = 0; i < newWord.Transl.Count; i++)
                            {
                                if (newWord.Transl[i].LngTo == strLngTo)
                                {
                                    newWord.Transl[i].Description = strTest;
                                   
                                    // tranh' tranh chap khi co 1 tu moi add vao queue
                                    if (WordViewModel.WordQueue.Count > 0 )
                                    {
                                        lock (WordViewModel.WordQueue)
                                        {
                                            
                                            WordViewModel.WordQueue.Remove(newWord.Name + "-" + newWord.LngFrom + "-" + newWord.Transl[0].LngTo);
                                            WordViewModel.WriteWordQueue();
                                            WordViewModel.WriteWordList();
                                        }
                                    }
                                    break;
                                }
                            }

                        });
                    }
                    
                }
                
            }
            catch (Exception ex)
            {
              //  Deployment.Current.Dispatcher.BeginInvoke(() => MessageBox.Show("You are offline 2 : " + ex.Message));
            }

        }
    }
}
